# PixPDF
# version: 1.0.0.2401
# author: github.com/gbroccoli

from pdf2image import convert_from_path
import os
import uuid
import sys
import json
import argparse
import zipfile
from datetime import datetime


def FilesZip(folder_path: str, zip_path: str) -> str:
    """Function to create a ZIP file from the contents of a folder.

    Args:
        folder_path (str): Path to the folder to be zipped.
        zip_path (str): Path to the output ZIP file.

    Returns:
        str: Path to the created ZIP file.
    """

    with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
        # Рекурсивно добавляем файлы из указанной директории в ZIP-архив
        for root, _, files in os.walk(folder_path):
            for file_name in files:
                file_path = os.path.join(root, file_name)
                # Исключаем файлы с расширением .zip из архивации
                if not file_name.endswith('.zip') and file_path.endswith('.pdf'):
                    # Определяем относительный путь в архиве
                    arcname = os.path.relpath(
                        file_path, start=folder_path)
                    zipf.write(file_path, arcname=arcname)

def pdf_to_images(pdf_path: str, output_folder: str = None, type_out_file: str = 'PNG') -> list:
    """Function for converting PDF to images

    Args:
        pdf_path (str): Path to the PDF file to be converted to images.
        output_folder (str): Path to the folder where the images will be stored.
        type_out_file (str): Type of the images that will be stored in the folder (default: 'PNG').

    Returns:
        list: List of paths to the generated image files.
    """

    if not pdf_path.endswith('.pdf'):
        print('Файл не является PDF-файлом')
        sys.exit(1)

    output_folder_dir = str(uuid.uuid4())

    if not output_folder:
        os.mkdir(output_folder_dir)
    else:
        if not os.path.exists(output_folder + '/' + output_folder_dir):
            os.makedirs(output_folder + '/' + output_folder_dir)

    images = convert_from_path(pdf_path)
    image_files = []

    for i, image in enumerate(images):
        image_file = os.path.join(output_folder, output_folder_dir, f'page_{i + 1}.{type_out_file.lower()}')
        image.save(image_file, type_out_file.upper())
        image_files.append(f'/files/output/{output_folder_dir}/{image_file}')

    zip_file_name = f'{output_folder_dir}.zip'
    zip_file_path = os.path.join(output_folder,output_folder_dir, zip_file_name)
    FilesZip(output_folder, zip_file_path)

    return {
        "image_files": image_files,
        "zip_file": f'/files/output/{output_folder}/{output_folder}.zip'
    }


def main():
    parser = argparse.ArgumentParser(
        prog='PixPDF',
        description='Утилита для преобразования pdf в изображения',
        epilog=f"Copyright (c) {datetime.now().year} https://gitlab.com/gbroccoli",
    )

    parser.add_argument(
        '--path', help='Путь к pdf файлу', required=True, type=str)
    parser.add_argument(
        '--out_path', help='Путь к папке для сохранения изображений', required=False)
    parser.add_argument(
        '--type_file', help='Тип изображения', default='PNG', required=False)

    args = parser.parse_args()

    output = pdf_to_images(args.path, args.out_path, args.type_file)
    print(json.dumps(output))


if __name__ == "__main__":
    main()
